import SlideUnlock from './slidevalidate/SlideUnlock';
import Pagination from "./pager/Pagination";

SlideUnlock.install = function (Vue){
    Vue.component(SlideUnlock.name, SlideUnlock);
    Vue.component(Pagination.name,Pagination);
}

export  {
    SlideUnlock,Pagination
}