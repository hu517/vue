import Vue from "vue";
import App from "./App.vue";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import {SlideUnlock,Pagination} from "./mycomponents/index";
import router from './router'
import store from './store'
import VueParticles  from "vue-particles";

Vue.config.productionTip = false;

Vue.use(ElementUI);
Vue.use(SlideUnlock);
Vue.use(Pagination);
Vue.use(VueParticles)

// router.beforeEach((to, from, next) => {
//   if (to.path.toLowerCase().indexOf('/admin')>=0) {
//     if (false) { next() } else {
//       next('/login')
//     }
//   } else {
//     next()
//   }
// })

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount("#app");
