import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

const originalPush = Router.prototype.push;
// 解决路由重复点击错误
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/index",
      component: () => import("./views"),
      children: [
        {
          path: "/home",
          name: "home",
          component: Home,
        },
        {
          path: "/about",
          name: "about",
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () =>
            import(/* webpackChunkName: "about" */ "./views/About.vue"),
        },
        {
          path: "/news",
          name: "news",
          component: () => import("./views/News.vue"),
        },
        {
          path: "/newsdetail/:id",
          name: "newsdetail",
          component: () => import("./views/NewsDetail.vue"),
        },
        {
          path: "/product",
          name: "product",
          component: () => import("./views/Product.vue"),
        },
        {
          path: "/productdetail/:id",
          name: "productdetail",
          component: () => import("./views/ProductDetail.vue"),
        },
        {
          path: "/contact",
          name: "contact",
          component: () => import("./views/Contact.vue"),
        },
        {
          path: "/",
          redirect: "/home",
        },
      ],
    },
    {
      path: "/admin",
      components: {
        admin: () => import("./views/admin/Layout"),
      },
      children: [{
        path: "index",
        name: "adminindex",
        component: () => import("./views/admin/Index"),
      }, {
        path: "news",
        name: "adminnews",
        component: () => import("./views/admin/news"),
      },{
        path: "product",
        name: "adminproduct",
        component: () => import("./views/admin/product"),
      }, {
        path: "/",
        redirect: "index",
      }],
    },
    {
      path: "/login",
      name: "login",
      component: () => import("./views/admin/Login"),
    },
    {
      path: "*",
      redirect: "/index",
    },
  ],
});
