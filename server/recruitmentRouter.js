const express = require("express");
const { recruitment } = require("./db");
const dbContext = recruitment;

const router = express.Router();

// 获取所有有效招聘
router.get("/", (req, res) => {
  if (!req.user.id)
    return res.status(500).json({
      message: "发生异常：[用户状态失效]",
    });
  dbContext.recruitmentGetList((err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 获取所有招聘
router.get("/getlistall", (req, res) => {
  if (!req.user.id)
    return res.status(500).json({
      message: "发生异常：[用户状态失效]",
    });
  dbContext.recruitmentGetAll((err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 获取单个有效招聘
router.get("/getrecruitment", (req, res) => {
  dbContext.recruitmentGetSingle(req.body.id, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 获取单个招聘通过有效性
router.get("/getrecruitmentflag", (req, res) => {
  dbContext.recruitmentGetFlagSingle(req.body.id, req.body.flag, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 新增招聘
router.post("/addrecruitment", (req, res) => {
  const { position, positiondesc } = req.body;
  dbContext.recruitmentAdd(position, positiondesc, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "添加成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

//修改信息
router.put("/updaterecruitment", (req, res) => {
  const { id, position, positiondesc } = req.body;
  dbContext.recruitmentUpdate(id, position, positiondesc, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "修改成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

//修改信息状态
router.put("/updaterecruitmentflag", (req, res) => {
  const { id, flag } = req.body;
  dbContext.recruitmentUpdateFlag(id, flag, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "修改成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 删除信息
router.delete("/delrecruitment", (req, res) => {
  dbContext.recruitmentDel(req.body.id, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "删除成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

module.exports = router;
