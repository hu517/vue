const express = require("express");
const { product } = require("./db");
const dbContext = product;

const router = express();

// 获取所有显示产品
router.get("/", (req, res) => {
  if (!req.user.id)
    return res.status(500).json({
      message: "发生异常：[用户状态失效]",
    });
  dbContext.productGetList((err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 获取所有
router.get("/getlistall", (req, res) => {
  if (!req.user.id)
    return res.status(500).json({
      message: "发生异常：[用户状态失效]",
    });
  dbContext.productGetAll((err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 获取单个产品
router.get("/getproduct", (req, res) => {
  dbContext.productGetSingle(req.body.id, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 通过类型获取产品
router.get("/getproductbytype", (req, res) => {
  dbContext.productSearchByType(req.body.tid, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 通过用户获取产品
router.get("/getproductbyuid", (req, res) => {
  dbContext.productSearchByUser(req.body.uid, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 关键字查找产品
router.get("/query", (req, res) => {
  dbContext.productSearch(req.body.keywords, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 添加产品
router.post("/addproduct", (req, res) => {
  const { type, name, coverpic, piclist, description, uid } = req.body;
  dbContext.productAdd(
    type,
    name,
    coverpic,
    piclist,
    description,
    uid,
    (err) => {
      if (!err) {
        return res.status(200).json({
          message: "添加成功",
        });
      } else {
        return res.status(500).json({
          message: "发生异常：[" + err + "]",
        });
      }
    }
  );
});

// 修改产品
router.put("/updateproduct", (req, res) => {
  const { type, name, coverpic, piclist, description, showflag, id } = req.body;
  dbContext.productUpdate(
    type,
    name,
    coverpic,
    piclist,
    description,
    showflag,
    id,
    (err) => {
      if (!err) {
        return res.status(200).json({
          message: "修改成功",
        });
      } else {
        return res.status(500).json({
          message: "发生异常：[" + err + "]",
        });
      }
    }
  );
});

// 更新产品阅读量
router.put("/updateproductread", (req, res) => {
  dbContext.productUpdateRead(req.body.id, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "修改成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 更新产品状态
router.put("/updateproductflag", (req, res) => {
  dbContext.productUpdateFlag(req.body.id, req.body.flag, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "修改成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 删除产品
router.delete("/delproduct", (req, res) => {
  dbContext.productDel(req.body.id, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "删除成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

module.exports = router;
