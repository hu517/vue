CREATE TABLE IF NOT EXISTS users ( 
    id            INTEGER           PRIMARY KEY ON CONFLICT ABORT AUTOINCREMENT,
    name          VARCHAR( 2, 20 )  NOT NULL
                                    UNIQUE,
    account       VARCHAR( 2, 20 )  NOT NULL
                                    UNIQUE,
    password      VARCHAR( 0 )      NOT NULL,
    age           INTEGER,
    address       VARCHAR( 2, 50 ),
    phone         CHAR( 3, 18 ),
    createtime    DATETIME          DEFAULT ( datetime( 'now', 'localtime' )  ),
    updatetime    DATETIME          DEFAULT ( datetime( 'now', 'localtime' )  ),
    lastlogintime DATETIME          DEFAULT ( datetime( 'now', 'localtime' )  ) 
);

