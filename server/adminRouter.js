const express = require("express");
const { admin } = require("./db");
const bcryptjs = require("bcryptjs");
const dbContext = admin;

const router = express.Router();

// 获取列表
router.get("/", (req, res) => {
  dbContext.adminGetList(req.body.id, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 获取单个用户
router.get("/getadmin", (req, res) => {
  dbContext.adminGetSingle(req.body.id, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 查询用户
router.get("/query", (req, res) => {
  dbContext.adminSearch(req.body.keywords, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 过滤用户
router.get("/filter", (req, res) => {
  let { name, account, age, phone } = req.body;
  dbContext.adminFilter(name, account, age, phone, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 用户名查询
router.get("/searchname", (req, res) => {
  dbContext.adminSearchByName(req.body.name, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 账号查询
router.get("/searchaccount", (req, res) => {
  dbContext.adminSearchByAccount(req.body.account, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 删除用户
router.delete("/delete", (req, res) => {
  dbContext.adminDel(req.body.id, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "删除成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 修改用户
router.put("/update", (req, res) => {
  let { id, name, account, age, address, phone } = req.body;
  dbContext.adminUpdate(id, name, account, age, address, phone, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "修改成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 修改登录时间
router.put("/UpdateLastLoginTime", (req, res) => {
  dbContext.adminUpdateLastLoginTime(req.body.id, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "修改成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 修改密码
router.put("/adminUpdatePassword", (req, res) => {
  let { id, password } = req.body;
  dbContext.adminUpdatePassword(id, bcryptjs.hashSync(password, 10), (err) => {
    if (!err) {
      return res.status(200).json({
        message: "修改成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

module.exports = router;
