const sqlite3 = require("sqlite3");
const db = new sqlite3.Database(__dirname + "/test.db");
const fs = require("fs");
const sqlMap = require("./sqlMap");

// 初始化数据库
fs.readFile(__dirname + "/initialization.sql", (err, data) => {
  if (!err) db.exec(data.toString());
});
// sql预编译
const stmt = (sql) => {
  return db.prepare(sql);
};

const admin = {
  // 管理员操作（需要授权）
  // 获取所有
  adminGetList: ($id, callback) => {
    stmt(sqlMap.getAdminList).all({ $id }, (err, data) => {
      callback(err, data);
    });
  },
  // 获取单个
  adminGetSingle: ($id, callback) => {
    stmt(sqlMap.getAdmin).get({ $id }, (err, data) => {
      callback(err, data);
    });
  },
  // 检索
  adminSearch: ($keywords, callback) => {
    stmt(sqlMap.searchAdmin).all({ $keywords }, (err, data) => {
      callback(err, data);
    });
  },
  // 过滤
  adminFilter: ($name, $account, $age, $phone, callback) => {
    stmt(sqlMap.filterAdmin).all(
      { $account, $name, $age, $phone },
      (err, data) => {
        callback(err, data);
      }
    );
  },
  // 按名字检索
  adminSearchByName: ($name, callback) => {
    stmt(sqlMap.getAdminByName).all({ $name }, (err, data) => {
      callback(err, data);
    });
  },
  // 按账号检索
  adminSearchByAccount: ($account, callback) => {
    stmt(sqlMap.getAdminByAccount).all({ $account }, (err, data) => {
      callback(err, data);
    });
  },
  // 删除用户
  adminDel: ($id, callback) => {
    stmt(sqlMap.delAdmin).run({ $id }, (err) => {
      callback(err);
    });
  },
  // 修改用户
  adminUpdate: ($id, $name, $account, $age, $address, $phone, callback) => {
    stmt(sqlMap.updateAdmin).run(
      { $id, $name, $account, $age, $address, $phone },
      (err) => {
        callback(err);
      }
    );
  },
  // 修改登录时间
  adminUpdateLastLoginTime: ($id, callback) => {
    stmt(sqlMap.updateAdminLoginTime).run({ $id }, (err) => {
      callback(err);
    });
  },
  // 修改密码
  adminUpdatePassword: ($id, $password, callback) => {
    stmt(sqlMap.updateAdminPassword).run({ $id, $password }, (err) => {
      callback(err);
    });
  },
};

const articleCategory = {
  // 获取所有文章类别
  articleCategoryGetList: (callback) => {
    stmt(sqlMap.getArticleCategoryList).all({}, (err, data) => {
      callback(err, data);
    });
  },
  articleCategoryGetListByPid: ($pid, callback) => {
    stmt(sqlMap.getArticleCategoryListByPid).all({ $pid }, (err, data) => {
      callback(err, data);
    });
  },
  articleCategoryGetTopLevelList: (callback) => {
    stmt(sqlMap.getTopLevelArticleCategoryList).all({}, (err, data) => {
      callback(err, data);
    });
  },
  articleCategoryGetSingle: ($id, callback) => {
    stmt(sqlMap.getArticleCategory).get({ $id }, (err, data) => {
      callback(err, data);
    });
  },
  articleCategoryAdd: ($pid, $name, callback) => {
    stmt(sqlMap.addArticleCategory).run({ $pid, $name }, (err) => {
      callback(err);
    });
  },
  articleCategoryUpdate: ($id, $pid, $name, callback) => {
    stmt(sqlMap.updateArticleCategory).run({ $id, $name, $pid }, (err) => {
      callback(err);
    });
  },
  articleCategoryDel: ($id, callback) => {
    stmt(sqlMap.delArticleCategory).run({ $id }, (err) => {
      callback(err);
    });
  },
};

const productCategory = {
  // 获取所有文章类别
  productCategoryGetList: (callback) => {
    stmt(sqlMap.getProductCategoryList).all({}, (err, data) => {
      callback(err, data);
    });
  },
  productCategoryGetListByPid: ($pid, callback) => {
    stmt(sqlMap.getProductCategoryListByPid).all({ $pid }, (err, data) => {
      callback(err, data);
    });
  },
  productCategoryGetTopLevelList: (callback) => {
    stmt(sqlMap.getTopLevelProductCategoryList).all({}, (err, data) => {
      callback(err, data);
    });
  },
  productCategoryGetSingle: ($id, callback) => {
    stmt(sqlMap.getProductCategory).get({ $id }, (err, data) => {
      callback(err, data);
    });
  },
  productCategoryAdd: ($pid, $name, callback) => {
    stmt(sqlMap.addProductCategory).run({ $pid, $name }, (err) => {
      callback(err);
    });
  },
  productCategoryUpdate: ($id, $pid, $name, callback) => {
    stmt(sqlMap.updateProductCategory).run({ $id, $name, $pid }, (err) => {
      callback(err);
    });
  },
  productCategoryDel: ($id, callback) => {
    stmt(sqlMap.delProductCategory).run({ $id }, (err) => {
      callback(err);
    });
  },
};

const aboutus = {
  aboutusGetList: (callback) => {
    stmt(sqlMap.getAboutusList).all({}, (err, data) => {
      callback(err, data);
    });
  },
  aboutusGetAll: (callback) => {
    stmt(sqlMap.getAboutusAll).all({}, (err, data) => {
      callback(err, data);
    });
  },
  aboutusGetSingle: ($id, callback) => {
    stmt(sqlMap.getAboutus).get($id, (err, data) => {
      callback(err, data);
    });
  },
  aboutusGetFlagSingle: ($id, $delflag, callback) => {
    stmt(sqlMap.getAboutusFlag).get($id, $delflag, (err, data) => {
      callback(err, data);
    });
  },
  aboutusAdd: ($title, $companydesc, callback) => {
    stmt(sqlMap.addAboutus).run({ $title, $companydesc }, (err) => {
      callback(err);
    });
  },
  aboutusUpdate: ($id, $title, $companydesc, callback) => {
    stmt(sqlMap.updateAboutus).run({ $id, $title, $companydesc }, (err) => {
      callback(err);
    });
  },
  aboutusUpdateFlag: ($id, $delflag, callback) => {
    stmt(sqlMap.updateAboutusFlag).run({ $id, $delflag }, (err) => {
      callback(err);
    });
  },
  aboutusDel: ($id, callback) => {
    stmt(sqlMap.delAboutus).run({ $id }, (err) => {
      callback(err);
    });
  },
};

const recruitment = {
  recruitmentGetList: (callback) => {
    stmt(sqlMap.getRecruitmentList).all({}, (err, data) => {
      callback(err, data);
    });
  },
  recruitmentGetAll: (callback) => {
    stmt(sqlMap.getRecruitmentAll).all({}, (err, data) => {
      callback(err, data);
    });
  },
  recruitmentGetSingle: ($id, callback) => {
    stmt(sqlMap.getRecruitment).get($id, (err, data) => {
      callback(err, data);
    });
  },
  recruitmentGetFlagSingle: ($id, $delflag, callback) => {
    stmt(sqlMap.getRecruitmentFlag).get($id, $delflag, (err, data) => {
      callback(err, data);
    });
  },
  recruitmentAdd: ($position, $positiondesc, callback) => {
    stmt(sqlMap.addRecruitment).run({ $position, $positiondesc }, (err) => {
      callback(err);
    });
  },
  recruitmentUpdate: ($id, $position, $positiondesc, callback) => {
    stmt(sqlMap.updateRecruitment).run(
      { $id, $position, $positiondesc },
      (err) => {
        callback(err);
      }
    );
  },
  recruitmentUpdateFlag: ($id, $delflag, callback) => {
    stmt(sqlMap.updateRecruitmentFlag).run({ $id, $delflag }, (err) => {
      callback(err);
    });
  },
  recruitmentDel: ($id, callback) => {
    stmt(sqlMap.delRecruitment).run({ $id }, (err) => {
      callback(err);
    });
  },
};

const contact = {
  contactGetList: (callback) => {
    stmt(sqlMap.getContactList).all({}, (err, data) => {
      callback(err, data);
    });
  },
  contactGetSingle: ($id, callback) => {
    stmt(sqlMap.getContact).get({ $id }, (err, data) => {
      callback(err, data);
    });
  },
  contactAdd: (
    $companyname,
    $companytel,
    $companyphone,
    $companyemail,
    $companyaddress,
    $companymap,
    callback
  ) => {
    stmt(sqlMap.addContact).run(
      {
        $companyname,
        $companytel,
        $companyphone,
        $companyemail,
        $companyaddress,
        $companymap,
      },
      (err) => {
        callback(err);
      }
    );
  },
  contactUpdate: (
    $companyname,
    $companytel,
    $companyphone,
    $companyemail,
    $companyaddress,
    $companymap,
    $id,
    callback
  ) => {
    stmt(sqlMap.updateContact).run(
      {
        $companyname,
        $companytel,
        $companyphone,
        $companyemail,
        $companyaddress,
        $companymap,
        $id,
      },
      (err) => {
        callback(err);
      }
    );
  },
  contactDel: ($id, callback) => {
    stmt(sqlMap.delContact).run({ $id }, (err) => {
      callback(err);
    });
  },
};

const article = {
  articleGetList: (callback) => {
    stmt(sqlMap.getArticleList).all({}, (err, data) => {
      callback(err, data);
    });
  },
  articleGetAll: (callback) => {
    stmt(sqlMap.getArticleAll).all({}, (err, data) => {
      callback(err, data);
    });
  },
  articleGetSingle: ($id, callback) => {
    stmt(sqlMap.getArticle).get({ $id }, (err, data) => {
      callback(err, data);
    });
  },
  articleSearchByType: ($type, callback) => {
    stmt(sqlMap.getArticleByTypeId).all({ $type }, (err, data) => {
      callback(err, data);
    });
  },
  articleSearchByUser: ($uid, callback) => {
    stmt(sqlMap.getArticleByUid).all({ $uid }, (err, data) => {
      callback(err, data);
    });
  },
  articleSearch: ($keywords, callback) => {
    stmt(sqlMap.searchArticle).all({ $keywords }, (err, data) => {
      callback(err, data);
    });
  },
  articleAdd: ($type, $title, $content, $coverpic, $uid, callback) => {
    stmt(sqlMap.addArticle).run(
      { $type, $title, $content, $coverpic, $uid },
      (err) => {
        callback(err);
      }
    );
  },
  articleUpdate: (
    $type,
    $title,
    $content,
    $coverpic,
    $showflag,
    $id,
    callback
  ) => {
    stmt(sqlMap.updateArticle).run(
      { $type, $title, $content, $coverpic, $showflag, $id },
      (err) => {
        callback(err);
      }
    );
  },
  articleUpdateHits: ($id, callback) => {
    stmt(sqlMap.updateArticleHits).run({ $id }, (err) => {
      callback(err);
    });
  },
  articleUpdateFlag: ($id, $showflag, callback) => {
    stmt(sqlMap.updateArticleFlag).run({ $showflag, $id }, (err) => {
      callback(err);
    });
  },
  articleDel: ($id, callback) => {
    stmt(sqlMap.delArticle).run({ $id }, (err) => {
      callback(err);
    });
  },
};

const product = {
  productGetList: (callback) => {
    stmt(sqlMap.getProductList).all({}, (err, data) => {
      callback(err, data);
    });
  },
  productGetAll: (callback) => {
    stmt(sqlMap.getProductAll).all({}, (err, data) => {
      callback(err, data);
    });
  },
  productGetSingle: ($id, callback) => {
    stmt(sqlMap.getProduct).get({ $id }, (err, data) => {
      callback(err, data);
    });
  },
  productSearchByType: ($type, callback) => {
    stmt(sqlMap.getProductByTypeId).all({ $type }, (err, data) => {
      callback(err, data);
    });
  },
  productSearchByUser: ($uid, callback) => {
    stmt(sqlMap.getProductByUid).all({ $uid }, (err, data) => {
      callback(err, data);
    });
  },
  productSearch: ($keywords, callback) => {
    stmt(sqlMap.searchProduct).all({ $keywords }, (err, data) => {
      callback(err, data);
    });
  },
  productAdd: (
    $type,
    $name,
    $coverpic,
    $piclist,
    $description,
    $uid,
    callback
  ) => {
    stmt(sqlMap.addProduct).run(
      { $type, $name, $coverpic, $piclist, $description, $uid },
      (err) => {
        callback(err);
      }
    );
  },
  productUpdate: (
    $type,
    $name,
    $coverpic,
    $piclist,
    $description,
    $showflag,
    $id,
    callback
  ) => {
    stmt(sqlMap.updateProduct).run(
      {
        $type,
        $name,
        $coverpic,
        $piclist,
        $description,
        $showflag,
        $id,
      },
      (err) => {
        callback(err);
      }
    );
  },
  productUpdateFlag: ($id, $showflag, callback) => {
    stmt(sqlMap.updateProductFlag).run({ $showflag, $id }, (err) => {
      callback(err);
    });
  },
  productUpdateRead: ($id, callback) => {
    stmt(sqlMap.updateProductRead).run({ $id }, (err) => {
      callback(err);
    });
  },
  productDel: ($id, callback) => {
    stmt(sqlMap.delProduct).run({ $id }, (err) => {
      callback(err);
    });
  },
};

module.exports = {
  // 常用操作授权
  auth: ($id, $name, callback) => {
    stmt(sqlMap.adminauth).get({ $id, $name }, (err, data) => {
      callback(err, data);
    });
  },
  // 常用操作登录
  login: ($account, callback) => {
    stmt(sqlMap.adminLogin).get({ $account }, (err, data) => {
      callback(err, data);
    });
  },
  // 常用操作注册（需要授权）
  register: ($name, $account, $password, callback) => {
    stmt(sqlMap.adminRegister).run({ $name, $account, $password }, (err) => {
      callback(err);
    });
  },
  admin,
  articleCategory,
  productCategory,
  aboutus,
  recruitment,
  contact,
  article,
  product,
};
