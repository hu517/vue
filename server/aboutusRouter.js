const express = require("express");
const { aboutus } = require("./db");
const dbContext = aboutus;

const router = express.Router();

// 获取所有有效信息
router.get("/", (req, res) => {
  if (!req.user.id)
    return res.status(500).json({
      message: "发生异常：[用户状态失效]",
    });
  dbContext.aboutusGetList((err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 获取所有信息
router.get("/getlistall", (req, res) => {
  if (!req.user.id)
    return res.status(500).json({
      message: "发生异常：[用户状态失效]",
    });
  dbContext.aboutusGetAll((err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 获取单个有效信息
router.get("/getaboutus", (req, res) => {
  dbContext.aboutusGetSingle(req.body.id, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 获取单个信息通过有效性
router.get("/getaboutusflag", (req, res) => {
  dbContext.aboutusGetFlagSingle(req.body.id, req.body.flag, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 新增信息
router.post("/addaboutus", (req, res) => {
  const { title, desc } = req.body;
  dbContext.aboutusAdd(title, desc, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "添加成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

//修改信息
router.put("/updateaboutus", (req, res) => {
  const { id, title, desc } = req.body;
  dbContext.aboutusUpdate(id, title, desc, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "修改成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

//修改信息状态
router.put("/updateaboutusflag", (req, res) => {
  const { id, flag } = req.body;
  dbContext.aboutusUpdateFlag(id, flag, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "修改成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 删除信息
router.delete("/delaboutus", (req, res) => {
  dbContext.aboutusDel(req.body.id, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "删除成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

module.exports = router;
