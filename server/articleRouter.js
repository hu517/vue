const express=require("express")
const {article}=require("./db")
const dbContext=article

const router=express.Router()

// 获取所有显示文章
router.get("/",(req,res)=>{
    if (!req.user.id)
    return res.status(500).json({
      message: "发生异常：[用户状态失效]",
    });
    dbContext.articleGetList((err,data)=>{
        if (!err) {
            return res.status(200).json({
              message: "查询成功",
              data,
            });
          } else {
            return res.status(500).json({
              message: "发生异常：[" + err + "]",
            });
          }
    })
})

// 获取所有
router.get("/getlistall",(req,res)=>{
    if (!req.user.id)
    return res.status(500).json({
      message: "发生异常：[用户状态失效]",
    });
    dbContext.articleGetAll((err,data)=>{
        if (!err) {
            return res.status(200).json({
              message: "查询成功",
              data,
            });
          } else {
            return res.status(500).json({
              message: "发生异常：[" + err + "]",
            });
          }
    })
})

// 获取单个文章
router.get("/getarticle",(req,res)=>{
    dbContext.articleGetSingle(req.body.id,(err,data)=>{
        if (!err) {
            return res.status(200).json({
              message: "查询成功",
              data,
            });
          } else {
            return res.status(500).json({
              message: "发生异常：[" + err + "]",
            });
          }
    })
})

// 通过类型获取文章
router.get("/getarticlebytype",(req,res)=>{
    dbContext.articleSearchByType(req.body.tid,(err,data)=>{
        if (!err) {
            return res.status(200).json({
              message: "查询成功",
              data,
            });
          } else {
            return res.status(500).json({
              message: "发生异常：[" + err + "]",
            });
          }
    })
})

// 通过用户获取文章
router.get("/getarticlebyuid",(req,res)=>{
    dbContext.articleSearchByUser(req.body.uid,(err,data)=>{
        if (!err) {
            return res.status(200).json({
              message: "查询成功",
              data,
            });
          } else {
            return res.status(500).json({
              message: "发生异常：[" + err + "]",
            });
          }
    })
})

// 关键字查找文章
router.get("/query",(req,res)=>{
    dbContext.articleSearch(req.body.keywords,(err,data)=>{
        if (!err) {
            return res.status(200).json({
              message: "查询成功",
              data,
            });
          } else {
            return res.status(500).json({
              message: "发生异常：[" + err + "]",
            });
          }
    })
})

// 添加文章
router.post("/addarticle",(req,res)=>{
    const {type,title,content,coverpic,uid}=req.body;
    dbContext.articleAdd(type,title,content,coverpic,uid,(err)=>{
        if (!err) {
            return res.status(200).json({
              message: "添加成功",
            });
          } else {
            return res.status(500).json({
              message: "发生异常：[" + err + "]",
            });
          }
    })
})

// 修改文章
router.put("/updatearticle",(req,res)=>{
    const {type,title,content,coverpic,showflag,id}=req.body;
    dbContext.articleUpdate(type,title,content,coverpic,showflag,id,(err)=>{
        if (!err) {
            return res.status(200).json({
              message: "修改成功",
            });
          } else {
            return res.status(500).json({
              message: "发生异常：[" + err + "]",
            });
          }
    })
})

// 更新文章热度
router.put("/updateartilehits",(req,res)=>{
    dbContext.articleUpdateHits(req.body.id,(err)=>{
        if (!err) {
            return res.status(200).json({
              message: "修改成功",
            });
          } else {
            return res.status(500).json({
              message: "发生异常：[" + err + "]",
            });
          }
    })
})

// 更新文章状态
router.put("/updateartileflag",(req,res)=>{
    dbContext.articleUpdateFlag(req.body.id,req.body.flag,(err)=>{
        if (!err) {
            return res.status(200).json({
              message: "修改成功",
            });
          } else {
            return res.status(500).json({
              message: "发生异常：[" + err + "]",
            });
          }
    })
})

// 删除文章
router.delete("/delarticle",(req,res)=>{
    dbContext.articleDel(req.body.id,(err)=>{
        if (!err) {
            return res.status(200).json({
              message: "删除成功",
            });
          } else {
            return res.status(500).json({
              message: "发生异常：[" + err + "]",
            });
          }
    })
})

module.exports=router