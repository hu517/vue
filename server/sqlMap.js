let sqlMap = {
  // 用户相关
  adminauth:'SELECT * FROM users WHERE id=$id AND name=$name;',
  adminLogin: `SELECT * FROM users WHERE account = $account;`,
  adminRegister: `INSERT INTO users( name, account, password) VALUES( $name, $account, $password);`,
  getAdminList: "SELECT * FROM users WHERE id!=$id;",
  getAdmin: "SELECT * FROM users WHERE id=$id;",
  getAdminByName: "SELECT * FROM users WHERE name LIKE '%'||$name||'%';",
  getAdminByAccount: "SELECT * FROM users WHERE account LIKE '%'||$account||'%';",
  searchAdmin: "SELECT * FROM users WHERE name LIKE '%'||$keywords||'%' OR account LIKE '%'||$keywords||'%' OR age LIKE '%'||$keywords||'%' OR phone LIKE '%'||$keywords||'%';",
  filterAdmin: "SELECT * FROM users WHERE name LIKE '%'||$name||'%' AND account LIKE '%'||$account||'%' AND age LIKE '%'||$age||'%' AND phone LIKE '%'||$phone||'%';",
  delAdmin: "DELETE FROM users WHERE id = $id;",
  updateAdmin: "UPDATE users SET name=$name,account=$account,age=$age,address=$address,phone=$phone,updatetime=datetime( 'now', 'localtime') WHERE id = $id;",
  updateAdminLoginTime: "UPDATE users SET lastlogintime = datetime( 'now', 'localtime') WHERE id = $id;",
  updateAdminPassword: "UPDATE users SET password = $password WHERE id = $id;",
  // 用户相关

  // 文章类别
  getArticleCategoryList:'SELECT * FROM category;',
  getArticleCategoryListByPid:'SELECT * FROM category WHERE pid = $pid;',
  getTopLevelArticleCategoryList:'SELECT * FROM category WHERE pid IS NULL;',
  getArticleCategory:'SELECT * FROM category WHERE id =$id;',
  addArticleCategory:'INSERT INTO category( pid , categoryname) VALUES( $pid , $name);',
  updateArticleCategory:"UPDATE category SET pid=$pid, categoryname=$name,updatetime=datetime( 'now', 'localtime') WHERE id=$id;",
  delArticleCategory:'DELETE FROM category WHERE id = $id;',
  // 文章类别

  // 产品类别
  getProductCategoryList:'SELECT * FROM productcategory;',
  getProductCategoryListByPid:'SELECT * FROM productcategory WHERE pid = $pid;',
  getTopLevelProductCategoryList:'SELECT * FROM productcategory WHERE pid IS NULL;',
  getProductCategory:'SELECT * FROM productcategory WHERE id =$id;',
  addProductCategory:'INSERT INTO productcategory( pid , categoryname) VALUES( $pid , $name);',
  updateProductCategory:"UPDATE productcategory SET pid=$pid, categoryname=$name,updatetime=datetime( 'now', 'localtime') WHERE id=$id;",
  delProductCategory:'DELETE FROM productcategory WHERE id = $id;',
  // 产品类别

  // 关于我们
  getAboutusList:'SELECT * FROM aboutus WHERE delflag = 0;', 
  getAboutusAll:'SELECT * FROM aboutus;', 
  getAboutus:'SELECT * FROM aboutus WHERE delflag = 0 AND id = $id;',
  getAboutusFlag:'SELECT * FROM aboutus WHERE delflag = $delflag AND id = $id;',
  addAboutus:'INSERT INTO aboutus( title, companydesc) VALUES ( $title, $companydesc );',
  updateAboutus:"UPDATE aboutus SET title = $title, companydesc = $companydesc,,updatetime=datetime( 'now', 'localtime') WHERE id = $id;",
  updateAboutusFlag:"UPDATE aboutus SET delflag = $delflag,updatetime=datetime( 'now', 'localtime') WHERE id = $id;",
  delAboutus:'DELETE FROM aboutus WHERE id = $id;',
  // 关于我们

  // 招聘
  getRecruitmentList:'SELECT * FROM recruitment WHERE delflag = 0;', 
  getRecruitmentAll:'SELECT * FROM recruitment;', 
  getRecruitment:'SELECT * FROM recruitment WHERE delflag = 0 AND id = $id;',
  getRecruitmentFlag:'SELECT * FROM recruitment WHERE delflag = $delflag AND id = $id;',
  addRecruitment:'INSERT INTO recruitment( position, positiondesc) VALUES ( $position, $positiondesc );',
  updateRecruitment:"UPDATE recruitment SET position = $position, positiondesc = $positiondesc,updatetime=datetime( 'now', 'localtime') WHERE id = $id;",
  updateRecruitmentFlag:"UPDATE recruitment SET delflag = $delflag,updatetime=datetime( 'now', 'localtime') WHERE id = $id;",
  delRecruitment:'DELETE FROM recruitment WHERE id = $id;',
  // 招聘

  // 联系我们
  getContactList:'SELECT * FROM contact;',
  getContact:'SELECT * FROM contact WHERE id=$id;',
  addContact:'INSERT INTO contact( companyname, companytel, companyphone, companyemail, companyaddress, companymap) VALUES ( $companyname, $companytel, $companyphone, $companyemail, $companyaddress, $companymap );',
  updateContact:"UPDATE contact SET companyname = $companyname, companytel = $companytel, companyphone = $companyphone, companyemail = $companyemail, companyaddress = $companyaddress, companymap = $companymap, updatetime = datetime( 'now', 'localtime') WHERE id = $id;",
  delContact:'DELETE FROM contact WHERE id = $id;',
  // 联系我们

  // 文章
  getArticleList:'SELECT * FROM article WHERE showflag = 1;',
  getArticleAll:'SELECT * FROM article;',
  getArticlePage:"SELECT * FROM article WHERE showflag = 1 limit $start,$end;",//分页
  getArticle:"SELECT * FROM article WHERE id = $id;",
  getArticleByTypeId:"SELECT * FROM article WHERE type = $type;",
  getArticleByUid:"SELECT * FROM article WHERE uid = $uid;",
  searchArticle:"SELECT * FROM article WHERE title LIKE '%'||$keywords||'%' OR content LIKE '%'||$keywords||'%';",
  addArticle:"INSERT INTO article( type, title, content, coverpic, uid) VALUES ( $type, $title, $content, $coverpic, $uid );",
  updateArticle:"UPDATE article SET type = $type, title = $title, content = $content, coverpic = $coverpic, showflag = $showflag, updatetime = datetime( 'now', 'localtime') WHERE id=$id;",
  updateArticleFlag:"UPDATE article SET showflag = $showflag WHERE id=$id;",
  updateArticleHits:"UPDATE article SET hits = hits+1 WHERE id=$id;",
  delArticle:"DELETE FROM article WHERE id = $id;",
  // 文章

  // 产品
  getProductList:'SELECT * FROM products WHERE showflag = 1;',
  getProductAll:'SELECT * FROM products;',
  getProduct:'SELECT * FROM products WHERE id = $id;',
  getProductByTypeId:"SELECT * FROM products WHERE type = $type;",
  getProductByUid:"SELECT * FROM products WHERE uid = $uid;",
  searchProduct:"SELECT * FROM products WHERE name LIKE '%'||$keywords||'%' OR description LIKE '%'||$keywords||'%';",
  addProduct:"INSERT INTO products( type, name, coverpic, piclist, description, uid) VALUES ( $type, $name, $coverpic, $piclist, $description, $uid );",
  updateProduct:"UPDATE products SET type = $type, name = $name, coverpic = $coverpic, piclist = $piclist, description = $description, showflag = $showflag, updatetime = datetime( 'now', 'localtime') WHERE id = $id;",
  updateProductFlag:"UPDATE products SET showflag = $showflag WHERE id=$id;",
  updateProductRead:"UPDATE products SET readcount = readcount+1 WHERE id=$id;",
  delProduct:"DELETE FROM products WHERE id = $id;",
  // 产品
};

module.exports = sqlMap;
