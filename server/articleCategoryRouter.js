const express = require("express");
const { articleCategory } = require("./db");
const dbContext = articleCategory;

const router = express.Router();

// 获取所有列表
router.get("/", (req, res) => {
  if (!req.user.id)
    return res.status(500).json({
      message: "发生异常：[用户状态失效]",
    });
  dbContext.articleCategoryGetList((err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 获取所有列表通过pid
router.get("/getlistbypid", (req, res) => {
  dbContext.articleCategoryGetListByPid(req.body.pid, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 获取所有顶级类别
router.get("/gettoplist", (req, res) => {
  dbContext.articleCategoryGetTopLevelList((err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 获取单个类别
router.get("/getcategory", (req, res) => {
  if (!req.user.id)
    return res.status(500).json({
      message: "发生异常：[用户状态失效]",
    });
  dbContext.articleCategoryGetSingle(req.body.id, (err, data) => {
    if (!err) {
      return res.status(200).json({
        message: "查询成功",
        data,
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 新增类别
router.post("/addcategory", (req, res) => {
  const { pid, name } = req.body;
  dbContext.articleCategoryAdd(pid, name, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "添加成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

//修改类别
router.put("/updatecategory", (req, res) => {
  const { id, name, pid } = req.body;
  dbContext.articleCategoryUpdate(id, pid, name, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "修改成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

// 删除类别
router.delete("/delcategory", (req, res) => {
  dbContext.articleCategoryDel(req.body.id, (err) => {
    if (!err) {
      return res.status(200).json({
        message: "删除成功",
      });
    } else {
      return res.status(500).json({
        message: "发生异常：[" + err + "]",
      });
    }
  });
});

module.exports = router;
