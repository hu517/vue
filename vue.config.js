module.exports = {
    devServer: {
        port: 8080,
            disableHostCheck: true,
            proxy: {
            '/ajax': {
                target: 'http://localhost:3000',
                // ws: true,
                changeOrigin: true,
                pathRewrite: {
                    // 标识替换
                    // 原请求地址为 /api/getData 将'/api'替换''时，
                    // 代理后的请求地址为： http://xxx.xxx.xxx/getData
                    // 若替换为'/other',则代理后的请求地址为 http://xxx.xxx.xxx/other/getData     
                   '^/ajax': ''  
                 }
            },
            // '/api': {
            //     target: 'http://localhost:3000/api'
            // }
        }
    }
}